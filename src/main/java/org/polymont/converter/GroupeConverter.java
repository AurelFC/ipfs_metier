package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.configuration.Config;
import org.polymont.dtos.GroupeDTO;
import org.polymont.entities.Groupe;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GroupeConverter implements Converter<Groupe, GroupeDTO> {

	@Autowired
	private ModelMapper mapper;

	public GroupeConverter() {
		super();
	}

	@Override
	public GroupeDTO convertToDto(Groupe groupe) {
		log.info("Convert to dto Groupe");
		GroupeDTO groupeDao = this.mapper.map(groupe, GroupeDTO.class);
		groupeDao.setUtilisateurs(groupe.toString() + Config.URI_UTILISATEURS);
		groupeDao.setFichiers(groupe.toString() + Config.URI_FICHIERS);
		return groupeDao;
	}

	@Override
	public Groupe convertToEntity(GroupeDTO groupeDto) {
		log.info("Convert to entity Groupe");

		return this.mapper.map(groupeDto, Groupe.class);
	}

}

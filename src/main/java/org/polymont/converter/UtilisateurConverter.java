package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.configuration.Config;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Utilisateur;
import org.polymont.repositories.ProfilRepository;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class UtilisateurConverter implements Converter<Utilisateur, UtilisateurDTO> {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private ProfilRepository profilRepository;

	public UtilisateurConverter() {
		super();
	}

	@Override
	public UtilisateurDTO convertToDto(Utilisateur utilisateur) {
		log.info("Convert to dto Utilisateur metier " + utilisateur);
		// Mapper
		utilisateur.setAgences(null);
		utilisateur.setGroupes(null);
		UtilisateurDTO utilisateurDto = this.mapper.map(utilisateur, UtilisateurDTO.class);

		String utilisateurUri = utilisateur.toString();
		utilisateurDto.setFichiers(utilisateurUri + Config.URI_FICHIERS);
		utilisateurDto.setAgences(utilisateurUri + Config.URI_AGENCES);
		utilisateurDto.setGroupes(utilisateurUri + Config.URI_GROUPES);

		return utilisateurDto;
	}

	@Override
	public Utilisateur convertToEntity(UtilisateurDTO utilisateurDto) {
		log.info("Convert to entity Utilisateur metier " + utilisateurDto);
		Utilisateur utilisateur = this.mapper.map(utilisateurDto, Utilisateur.class);

		if (utilisateurDto.getProfil() != null) {
			utilisateur.setProfil(profilRepository.getOne(Config.getIdFromUri(utilisateurDto.getProfil())));
		}

		return utilisateur;
	}

}
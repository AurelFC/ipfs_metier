package org.polymont.converter;

import org.modelmapper.ModelMapper;
import org.polymont.configuration.Config;
import org.polymont.dtos.FichierDTO;
import org.polymont.entities.Fichier;
import org.polymont.repositories.AgenceRepository;
import org.polymont.repositories.GroupeRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class FichierConverter implements Converter<Fichier, FichierDTO> {

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Autowired
	private AgenceRepository agenceRepository;
	
	@Autowired
	private GroupeRepository groupeRepository;

	public FichierConverter() {
		super();
	}

	@Override
	public FichierDTO convertToDto(Fichier fichier) {
		log.info("Convert to dto Fichier " + fichier);
		FichierDTO fichierDto = this.mapper.map(fichier, FichierDTO.class);
		fichierDto.setHistorique(fichier.toString() + Config.URI_HISTORIQUES);
		return fichierDto;
	}

	@Override
	public Fichier convertToEntity(FichierDTO fichierDto) {
		log.info("Convert to entity Fichier");
		log.info("fichierDto " + fichierDto);
		Fichier fichier = this.mapper.map(fichierDto, Fichier.class);

		if (fichierDto.getOwner() != null) {
			String [] spl  = fichierDto.getOwner().split("/");
			if (spl[1].equals("utilisateurs")) {
				fichier.setOwner(utilisateurRepository.getOne(Config.getIdFromUri(fichierDto.getOwner())));
			}
			if (spl[1].equals("agences")) {
				fichier.setOwner(agenceRepository.getOne(Config.getIdFromUri(fichierDto.getOwner())));
			}
			if (spl[1].equals("groupes")) {
				fichier.setOwner(groupeRepository.getOne(Config.getIdFromUri(fichierDto.getOwner())));
			}
			
			
		}
		return fichier;
	}

}

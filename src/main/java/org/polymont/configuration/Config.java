package org.polymont.configuration;

import org.modelmapper.ModelMapper;
import org.polymont.converter.AgenceConverter;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.GroupeConverter;
import org.polymont.converter.HistoriqueConverter;
import org.polymont.converter.NotificationConverter;
import org.polymont.converter.PrivilegeConverter;
import org.polymont.converter.ProfilConverter;
import org.polymont.converter.TypeNotificationConverter;
import org.polymont.converter.UtilisateurConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class Config {

	public static final String URI_FICHIERS = "/fichiers/";
	public static final String URI_UTILISATEURS = "/utilisateurs/";
	public static final String URI_NOTIFICATIONS = "/notifications/";
	public static final String URI_GROUPES = "/groupes/";
	public static final String URI_AGENCES = "/agences/";
	public static final String URI_HISTORIQUES = "/historiques/";
	public static final String URI_PROFILS = "/profils/";
	public static final String URI_TYPENOTIFICATION = "/typeNotifications/";
	public static final String URI_PRIVILEGES = "/privileges/";

	public static long getIdFromUri(String uri) {
		// Uri == /xxxxxxxxxxx/id || Uri == /xxxxxxxxxxx/id/yyyyyyyyyy
		if (uri != null) {
			String[] tabUri = uri.split("/");
			// String[] = ["", "xxxxxxxxxxx", "id", "yyyyyyyyyy"]
			if (tabUri.length >= 3) {
				return Long.parseLong(tabUri[2]);
			}
			return 0l;
		} else {
			return 0l;
		}
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build();

	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public UtilisateurConverter utilisateurConverter() {
		return new UtilisateurConverter();
	}

	@Bean
	public FichierConverter fichierConverter() {
		return new FichierConverter();
	}

	@Bean
	public PrivilegeConverter privilegeConverter() {
		return new PrivilegeConverter();
	}

	@Bean
	public AgenceConverter agenceConverter() {
		return new AgenceConverter();
	}

	@Bean
	public GroupeConverter groupeConverter() {
		return new GroupeConverter();
	}

	@Bean
	public NotificationConverter notificationConverter() {
		return new NotificationConverter();
	}

	@Bean
	public TypeNotificationConverter typeNotificationConverter() {
		return new TypeNotificationConverter();
	}

	@Bean
	public HistoriqueConverter historiqueConverter() {
		return new HistoriqueConverter();
	}

	@Bean
	public ProfilConverter profilConverter() {
		return new ProfilConverter();
	}
}

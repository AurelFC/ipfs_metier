package org.polymont.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.HistoriqueConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.HistoriqueDTO;
import org.polymont.entities.Fichier;
import org.polymont.entities.Historique;
import org.polymont.entities.Utilisateur;
import org.polymont.repositories.FichierRepository;
import org.polymont.repositories.HistoriqueRepository;
import org.polymont.repositories.TypeNotificationRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/fichiers")
@CrossOrigin(origins = "*")
@Log4j2
public class FichierController {

	@Autowired
	FichierRepository fichierRepository;

	@Autowired
	FichierConverter fichierConverter;

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	HistoriqueRepository historiqueRepository;

	@Autowired
	HistoriqueConverter historiqueConverter;

	@Autowired
	TypeNotificationRepository typeNotificationRepository;

	public FichierController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FichierDTO> getAll() {
		List<FichierDTO> fichiersDtos = new ArrayList<>();
		fichierRepository.findAll().forEach(fichier -> fichiersDtos.add(fichierConverter.convertToDto(fichier)));
		log.info(fichiersDtos);
		return fichiersDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public FichierDTO getById(@PathVariable long id) {
		Fichier fichier = fichierRepository.getOne(id);
		return fichierConverter.convertToDto(fichier);
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter un fichier
	 * 
	 * @param FichierDTO newFichier
	 * @return ResponseEntity<Fichier>
	 */
	@PostMapping
	public FichierDTO add(@RequestBody FichierDTO newFichier) {
		log.info("POST Fichier - " + newFichier);

		// Enregistrement du fichier dans la base
		Fichier fichierEntity = fichierConverter.convertToEntity(newFichier);
		fichierEntity.setOwner(utilisateurRepository.getOne(Config.getIdFromUri(newFichier.getOwner())));
		fichierEntity = fichierRepository.save(fichierEntity);
		Historique newHisto = new Historique(Calendar.getInstance(), "CREATION", fichierEntity);
		historiqueRepository.save(newHisto);
		return fichierConverter.convertToDto(fichierEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'un fichier
	 * 
	 * @param long id - ID du fichier à mettre à jour
	 * @return ResponseEntity<Fichier>
	 */
	@PutMapping(path = "/{id}")
	public FichierDTO update(@RequestBody FichierDTO updateFichierDTO, @PathVariable long id) {
		log.info("PUT Fichier - " + updateFichierDTO);

		Fichier fichier = fichierRepository.getOne(id);
		Utilisateur utilisateur = new Utilisateur();
		List<Historique> historiques = new ArrayList<>();

		// Mise à jour du fichier
		fichier.setDateAjout(updateFichierDTO.getDateAjout());
		fichier.setExtension(updateFichierDTO.getExtension());
		fichier.setHash(updateFichierDTO.getHash());
		fichier.setNom(updateFichierDTO.getNom());
		fichier.setVersion(updateFichierDTO.getVersion());
		fichier.setHistoriques(historiques);
		if (fichier.getOwner() != null) {
			long idOwner = Config.getIdFromUri(updateFichierDTO.getOwner());
			utilisateur = utilisateurRepository.getOne(idOwner);
		}
		fichier.setOwner(utilisateur);

		return fichierConverter.convertToDto(fichierRepository.save(fichier));
	}

	/**
	 * Méthode getHistoriques : retourne les historiques concernant un fichier
	 * 
	 * @param id
	 * @return ResponseEntity<Historique>
	 */
	@GetMapping(path = "/{id}/historiques", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<HistoriqueDTO> getHistoriques(@PathVariable long id) {
		log.info("GET Historiques - ");
		List<HistoriqueDTO> historiques = new ArrayList<>();
		fichierRepository.getHistoriques(id)
				.forEach(historique -> historiques.add(historiqueConverter.convertToDto(historique)));
		return historiques;
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer un fichier
	 * 
	 * @param long id - ID du fichier à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Fichier - ");
		fichierRepository.delete(id);

	}

}

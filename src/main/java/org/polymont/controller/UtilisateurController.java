package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.AgenceConverter;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.GroupeConverter;
import org.polymont.converter.NotificationConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.AgenceDTO;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.GroupeDTO;
import org.polymont.dtos.NotificationDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Agence;
import org.polymont.entities.Groupe;
import org.polymont.entities.Utilisateur;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.polymont.repositories.AgenceRepository;
import org.polymont.repositories.GroupeRepository;
import org.polymont.repositories.ProfilRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/utilisateurs")
@CrossOrigin(origins = "*")
@Log4j2
public class UtilisateurController {

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	FichierConverter fichierConverter;

	@Autowired
	AgenceRepository agenceRepository;

	@Autowired
	AgenceConverter agenceConverter;

	@Autowired
	GroupeRepository groupeRepository;

	@Autowired
	GroupeConverter groupeConverter;

	@Autowired
	ProfilRepository profilRepository;

	@Autowired
	NotificationConverter notificationConverter;

	public UtilisateurController() {
		super();
	}

	/**
	 * Méthode GET de l'api RESTFull permettant de récupérer un utilisateur
	 * 
	 * @param long id - ID de l'utilisateur à récupérer
	 * @return UtilisateurDTO - ResponseEntity contenant l'utilisateur demandé
	 */
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public UtilisateurDTO getById(@PathVariable long id) {
		Utilisateur utilisateur;
		try {
			utilisateur = utilisateurRepository.getOne(id);
		} catch (RuntimeException e) {
		
			throw new GenericEntityNotFoundException(id, Utilisateur.class);
		}

		return utilisateurConverter.convertToDto(utilisateur);
	}

	/**
	 * Méthode GET de l'api RESTFull permettant de récupérer tous les utilisateurs
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UtilisateurDTO> getAll(@RequestParam(value = "email", required = false) String email) {
		List<UtilisateurDTO> utilisateursDtos = new ArrayList<>();
		if (email == null) {
			utilisateursDtos = getAllUtilisateurs();
		} else {
			try {
				utilisateursDtos.add(getByEmail(email));
			} catch (RuntimeException e) {
				throw new GenericEntityNotFoundException(email, Utilisateur.class);
			}

		}
		return utilisateursDtos;
	}

	public UtilisateurDTO getByEmail(String email) {
		return utilisateurConverter.convertToDto(utilisateurRepository.findByEmailPro(email));
	}

	public List<UtilisateurDTO> getAllUtilisateurs() {
		List<UtilisateurDTO> utilisateursDtos = new ArrayList<>();
		utilisateurRepository.findAll().forEach(util -> utilisateursDtos.add(utilisateurConverter.convertToDto(util)));
		log.info("UtilisateursDTOs" + utilisateursDtos);
		return utilisateursDtos;

	}

	/**
	 * Méthode getAgences : retourne les agences concernant un utilisateur
	 * 
	 * @param id
	 * @return ResponseEntity<Agence>
	 */
	@GetMapping(path = "/{id}/agences", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AgenceDTO> getAgences(@PathVariable long id) {
		log.info("GET Agences - ");
		List<AgenceDTO> agences = new ArrayList<>();
		utilisateurRepository.getAgences(id).forEach(agence -> agences.add(agenceConverter.convertToDto(agence)));
		return agences;
	}

	/**
	 * Méthode getGroupes : retourne les groupes concernant un utilisateur
	 * 
	 * @param id
	 * @return ResponseEntity<Agence>
	 */
	@GetMapping(path = "/{id}/groupes", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GroupeDTO> getGroupes(@PathVariable long id) {
		log.info("GET Groupes - ");
		List<GroupeDTO> groupes = new ArrayList<>();
		utilisateurRepository.getGroupes(id).forEach(groupe -> groupes.add(groupeConverter.convertToDto(groupe)));
		return groupes;
	}

	/**
	 * Méthode getNotifications : retourne les notifications concernant un
	 * utilisateur
	 * 
	 * @param id
	 * @return ResponseEntity<Notification>
	 */
	@GetMapping(path = "/{id}/notifications", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<NotificationDTO> getNotifications(@PathVariable long id) {
		log.info("GET Notifications - ");
		List<NotificationDTO> notifications = new ArrayList<>();
		utilisateurRepository.getNotifications(id)
				.forEach(notification -> notifications.add(notificationConverter.convertToDto(notification)));
		return notifications;
	}

	/**
	 * Méthode getFichiers : retourne les fichiers concernant un utilisateur
	 * 
	 * @param id
	 * @return ResponseEntity<Fichier>
	 */
	@GetMapping(path = "/{id}/fichiers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FichierDTO> getFichiers(@PathVariable long id) {
		log.info("GET Fichiers - ");
		List<FichierDTO> fichiers = new ArrayList<>();
		utilisateurRepository.getFichiers(id).forEach(fichier -> fichiers.add(fichierConverter.convertToDto(fichier)));
		return fichiers;
	}

	/**
	 * Méthode POST de l'api RESTFull pour ajouter un utilisateur
	 * 
	 * @param Employe newEmploye
	 * @return newEmploye
	 */
	@PostMapping
	public UtilisateurDTO add(@RequestBody UtilisateurDTO utilisateurDto) {
		log.info("ADD utilisateur METIER -----");
		Utilisateur utilisateurEntity = utilisateurConverter.convertToEntity(utilisateurDto);

		utilisateurEntity = utilisateurRepository.save(utilisateurEntity);

		return utilisateurConverter.convertToDto(utilisateurEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFull pour la modification d'un utilisateur
	 * 
	 * @param UtilisateurDTO update
	 * @param long           id - ID de l'utilisateur à mettre à jour
	 * @return UtilisateurDTO
	 */
	@PutMapping(path = "/{id}")
	public UtilisateurDTO update(@RequestBody(required = true) UtilisateurDTO updateUtilisateurDTO,
			@PathVariable long id) {

		Utilisateur utilisateur = utilisateurRepository.getOne(id);

		utilisateur.setNom(updateUtilisateurDTO.getNom());
		utilisateur.setPrenom(updateUtilisateurDTO.getPrenom());
		utilisateur.setEmailPro(updateUtilisateurDTO.getEmailPro());
		utilisateur.setPrenom(updateUtilisateurDTO.getPrenom());
		utilisateur.setTelPro(updateUtilisateurDTO.getTelPro());
		utilisateur.setProfil(profilRepository.getOne(Config.getIdFromUri(updateUtilisateurDTO.getProfil())));

		return utilisateurConverter.convertToDto(utilisateurRepository.save(utilisateur));
	}

	/**
	 * Méthode PUT pour ajouter un groupe à un utilisateur
	 * 
	 * @param id
	 * @param GroupeDTO
	 * @return
	 */

	@PutMapping(path = "/{id}/groupes", produces = MediaType.APPLICATION_JSON_VALUE)
	public UtilisateurDTO addGroupes(@PathVariable long id, @RequestBody GroupeDTO newGroupeDTO) {
		log.info("PUT Groupes ajout - ");
		Groupe groupe = groupeConverter.convertToEntity(newGroupeDTO);
		Utilisateur utilisateur = utilisateurRepository.addGroupe(id, groupe);
		return utilisateurConverter.convertToDto(utilisateur);
	}

	/**
	 * Méthode PUT pour ajouter un agence à un utilisateur
	 * 
	 * @param id
	 * @param AgenceDTO
	 * @return
	 */

	@PutMapping(path = "/{id}/agences", produces = MediaType.APPLICATION_JSON_VALUE)
	public UtilisateurDTO addAgences(@PathVariable long id, @RequestBody AgenceDTO newAgenceDTO) {
		log.info("PUT Agences ajout - ");
		Agence agence = agenceConverter.convertToEntity(newAgenceDTO);
		Utilisateur utilisateur = utilisateurRepository.addAgence(id, agence);
		return utilisateurConverter.convertToDto(utilisateur);
	}

	/**
	 * Méthode DELETE de l'api RESTFull permettant de supprimer un utilisateur
	 * 
	 * @param long id - ID de l'utilisateur à supprimer
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		utilisateurRepository.delete(id);
	}

	/**
	 * Méthode PUT permettant de retirer une agence à l'utilisateur
	 * 
	 * @param id  (correspond à l'id de l'utilisateur)
	 * @param id2 (correspond à l'id du groupe)
	 * @return
	 */
	@PutMapping(path = "/{id}/agences/{id2}")
	public AgenceDTO deleteAgence(@PathVariable long id, @PathVariable long id2) {
		log.info("DELETE agence de l'utilisateur - ");

		return agenceConverter.convertToDto(utilisateurRepository.deleteAgence(id, id2));
	}

	/**
	 * Méthode PUT permettant de retirer un groupe à l'utilisateur
	 * 
	 * @param id  (correspond à l'id l'utilisateur )
	 * @param id2 (correspond à l'id du groupe)
	 * @return
	 */
	@PutMapping(path = "/{id}/groupes/{id2}")
	public GroupeDTO deleteGroupe(@PathVariable long id, @PathVariable long id2) {
		log.info("DELETE groupe de l'utilisateur - ");
		return groupeConverter.convertToDto(utilisateurRepository.deleteGroupe(id, id2));
	}

}

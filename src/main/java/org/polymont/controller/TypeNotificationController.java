package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import org.polymont.converter.TypeNotificationConverter;
import org.polymont.dtos.TypeNotificationDTO;
import org.polymont.entities.TypeNotification;
import org.polymont.repositories.TypeNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/typeNotifications")
@CrossOrigin(origins = "*")
@Log4j2
public class TypeNotificationController {

	@Autowired
	TypeNotificationRepository typeNotificationRepository;

	@Autowired
	TypeNotificationConverter typeNotificationConverter;

	public TypeNotificationController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TypeNotificationDTO> getAll() {
		List<TypeNotificationDTO> typeNotificationsDtos = new ArrayList<>();
		typeNotificationRepository.findAll().forEach(typeNotification -> typeNotificationsDtos
				.add(typeNotificationConverter.convertToDto(typeNotification)));
		log.info(typeNotificationsDtos);
		return typeNotificationsDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public TypeNotificationDTO getById(@PathVariable long id) {
		TypeNotification privilege = typeNotificationRepository.getOne(id);
		return typeNotificationConverter.convertToDto(privilege);
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter une typenotification
	 * 
	 * @param TypeNotificationDTO newTypeNotification
	 * @return ResponseEntity<TypeNotification>
	 */
	@PostMapping
	public TypeNotificationDTO add(@RequestBody TypeNotificationDTO newTypeNotification) {
		log.info("POST TypeNotification - " + newTypeNotification);
		// Enregistrement du privilege dans la base
		TypeNotification typeNotificationEntity = typeNotificationRepository
				.save(typeNotificationConverter.convertToEntity(newTypeNotification));
		return typeNotificationConverter.convertToDto(typeNotificationEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'une typenotification
	 * 
	 * @param long id - ID du typenotification à mettre à jour
	 * @return ResponseEntity<TypeNotification>
	 */
	@PutMapping(path = "/{id}")
	public TypeNotificationDTO update(@RequestBody TypeNotificationDTO updateTypeNotificationDTO,
			@PathVariable long id) {
		log.info("PUT TypeNotification - " + updateTypeNotificationDTO);

		TypeNotification privilege = typeNotificationRepository.getOne(id);
		// Mise à jour de l'privilege
		privilege.setLibelle(updateTypeNotificationDTO.getLibelle());
		return typeNotificationConverter.convertToDto(typeNotificationRepository.save(privilege));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer une typenotification
	 * 
	 * @param long id - ID du typenotification à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE TypeNotification - ");
		typeNotificationRepository.delete(id);
	}

}

package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import org.polymont.converter.PrivilegeConverter;
import org.polymont.converter.ProfilConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.PrivilegeDTO;
import org.polymont.dtos.ProfilDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Privilege;
import org.polymont.entities.Profil;
import org.polymont.repositories.PrivilegeRepository;
import org.polymont.repositories.ProfilRepository;
import org.polymont.repositories.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/profils")
@CrossOrigin(origins = "*")
@Log4j2
public class ProfilController {

	@Autowired
	ProfilRepository profilRepository;

	@Autowired
	ProfilConverter profilConverter;

	@Autowired
	UtilisateurRepository utilisateurRepository;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	PrivilegeRepository privilegeRepository;

	@Autowired
	PrivilegeConverter privilegeConverter;

	public ProfilController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ProfilDTO> getAll() {
		List<ProfilDTO> profilsDtos = new ArrayList<>();
		profilRepository.findAll().forEach(profil -> profilsDtos.add(profilConverter.convertToDto(profil)));
		log.info(profilsDtos);
		return profilsDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProfilDTO getById(@PathVariable long id) {
		Profil profil = profilRepository.getOne(id);
		return profilConverter.convertToDto(profil);
	}

	/**
	 * Méthode getUtilisateurs : retourne les utilisateurs concernant un profil
	 * 
	 * @param id
	 * @return ResponseEntity<Utilisateur>
	 */
	@GetMapping(path = "/{id}/utilisateurs", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UtilisateurDTO> getUtilisateurs(@PathVariable long id) {
		log.info("GET Utilisateurs - ");
		List<UtilisateurDTO> utilisateurs = new ArrayList<>();
		profilRepository.getUtilisateurs(id)
				.forEach(utilisateur -> utilisateurs.add(utilisateurConverter.convertToDto(utilisateur)));
		return utilisateurs;
	}

	/**
	 * Méthode getPrivileges : retourne les agences concernant un utilisateur
	 * 
	 * @param id
	 * @return ResponseEntity<Agence>
	 */
	@GetMapping(path = "/{id}/privileges", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PrivilegeDTO> getPrivileges(@PathVariable long id) {
		log.info("GET Privileges - ");
		List<PrivilegeDTO> privileges = new ArrayList<>();
		profilRepository.getPrivileges(id)
				.forEach(privilege -> privileges.add(privilegeConverter.convertToDto(privilege)));
		return privileges;
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter un profil
	 * 
	 * @param ProfilDTO newProfil
	 * @return ResponseEntity<Profil>
	 */
	@PostMapping
	public ProfilDTO add(@RequestBody ProfilDTO newProfil) {
		log.info("POST Profil - " + newProfil);
		// Enregistrement du profil dans la base
		Profil profilEntity = profilRepository.save(profilConverter.convertToEntity(newProfil));
		return profilConverter.convertToDto(profilEntity);
	}

	/**
	 * Méthode POST pour ajouter un privilege à un profil
	 * 
	 * @param id
	 * @param newPrivilegeDTO
	 * @return
	 */

	@PostMapping(path = "/{id}/privileges", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProfilDTO addPrivileges(@PathVariable long id, @RequestBody PrivilegeDTO newPrivilegeDTO) {
		log.info("POST Privileges ajout - ");
		Profil profil = profilRepository.getOne(id);
		Privilege priv = privilegeConverter.convertToEntity(newPrivilegeDTO);
		if (priv.getId() == 0) {
			privilegeRepository.save(priv);
		}
		log.info("priv " + priv);
		profil.addPrivileges(priv);
		profil = profilRepository.save(profil);
		log.info("profil apres save and flush " + profil);
		List<PrivilegeDTO> privileges = new ArrayList<>();
		profil.getPrivileges().forEach(privilege -> privileges.add(privilegeConverter.convertToDto(privilege)));
		return profilConverter.convertToDto(profil);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'un profil
	 * 
	 * @param long id - ID du profil à mettre à jour
	 * @return ResponseEntity<Profil>
	 */
	@PutMapping(path = "/{id}")
	public ProfilDTO update(@RequestBody ProfilDTO updateProfilDTO, @PathVariable long id) {
		log.info("PUT Profil - " + updateProfilDTO);

		Profil profil = profilRepository.getOne(id);
		List<PrivilegeDTO> privileges = new ArrayList<>();
		privileges.addAll(getPrivileges(id));
		log.info(privileges);
		// Mise à jour du profil
		profil.setLibelle(updateProfilDTO.getLibelle());

		return profilConverter.convertToDto(profilRepository.save(profil));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer un profil
	 * 
	 * @param long id - ID du profil à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Profil - ");
		profilRepository.delete(id);
	}

}

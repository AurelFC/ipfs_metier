package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import org.polymont.converter.PrivilegeConverter;
import org.polymont.dtos.PrivilegeDTO;
import org.polymont.entities.Privilege;
import org.polymont.repositories.PrivilegeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/privileges")
@CrossOrigin(origins = "*")
@Log4j2
public class PrivilegeController {

	@Autowired
	PrivilegeRepository privilegeRepository;

	@Autowired
	PrivilegeConverter privilegeConverter;

	public PrivilegeController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PrivilegeDTO> getAll() {
		List<PrivilegeDTO> privilegesDtos = new ArrayList<>();
		privilegeRepository.findAll().forEach(action -> privilegesDtos.add(privilegeConverter.convertToDto(action)));
		log.info(privilegesDtos);
		return privilegesDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public PrivilegeDTO getById(@PathVariable long id) {
		Privilege privilege = privilegeRepository.getOne(id);
		return privilegeConverter.convertToDto(privilege);
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter un privilege
	 * 
	 * @param PrivilegeDTO newPrivilege
	 * @return ResponseEntity<Privilege>
	 */
	@PostMapping
	public PrivilegeDTO add(@RequestBody PrivilegeDTO newPrivilege) {
		log.info("POST Privilege - " + newPrivilege);
		// Enregistrement du privilege dans la base
		Privilege privilegeEntity = privilegeRepository.save(privilegeConverter.convertToEntity(newPrivilege));
		return privilegeConverter.convertToDto(privilegeEntity);
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'un privilege
	 * 
	 * @param long id - ID du privilege à mettre à jour
	 * @return ResponseEntity<Privilege>
	 */
	@PutMapping(path = "/{id}")
	public PrivilegeDTO update(@RequestBody PrivilegeDTO updatePrivilegeDTO, @PathVariable long id) {
		log.info("PUT Privilege - " + updatePrivilegeDTO);

		Privilege privilege = privilegeRepository.getOne(id);
		// Mise à jour de l'privilege
		privilege.setLibelle(updatePrivilegeDTO.getLibelle());
		return privilegeConverter.convertToDto(privilegeRepository.save(privilege));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer un privilege
	 * 
	 * @param long id - ID du privilege à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Privilege - ");
		privilegeRepository.delete(id);
	}

}

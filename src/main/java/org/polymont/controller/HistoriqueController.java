package org.polymont.controller;

import java.util.ArrayList;
import java.util.List;

import org.polymont.converter.HistoriqueConverter;
import org.polymont.converter.TypeNotificationConverter;
import org.polymont.dtos.HistoriqueDTO;
import org.polymont.entities.Historique;
import org.polymont.repositories.HistoriqueRepository;
import org.polymont.repositories.TypeNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(path = "/historiques")
@CrossOrigin(origins = "*")
@Log4j2
public class HistoriqueController {

	@Autowired
	HistoriqueRepository historiqueRepository;

	@Autowired
	HistoriqueConverter historiqueConverter;

	@Autowired
	TypeNotificationRepository typeNotificationRepository;

	@Autowired
	TypeNotificationConverter typeNotificationConverter;

	public HistoriqueController() {
		super();
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<HistoriqueDTO> getAll() {
		List<HistoriqueDTO> historiquesDtos = new ArrayList<>();
		historiqueRepository.findAll()
				.forEach(historique -> historiquesDtos.add(historiqueConverter.convertToDto(historique)));
		log.info(historiquesDtos);
		return historiquesDtos;

	}

	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public HistoriqueDTO getById(@PathVariable long id) {
		Historique historique = historiqueRepository.getOne(id);
		return historiqueConverter.convertToDto(historique);
	}

	/**
	 * Méthode POST de l'api RESTFul pour ajouter une historique
	 * 
	 * @param HistoriqueDTO newHistorique
	 * @return ResponseEntity<Historique>
	 */
	@PostMapping
	public HistoriqueDTO add(@RequestBody HistoriqueDTO newHistorique) {
		log.info("POST Historique - " + newHistorique);
		// Enregistrement de l'historique dans la base
		return historiqueConverter
				.convertToDto(historiqueRepository.save(historiqueConverter.convertToEntity(newHistorique)));
	}

	/**
	 * Méthode PUT de l'api RESTFul pour la modification d'une historique
	 * 
	 * @param long id - ID de l'historique à mettre à jour
	 * @return ResponseEntity<Historique>
	 */
	@PutMapping(path = "/{id}")
	public HistoriqueDTO update(@RequestBody HistoriqueDTO updateHistoriqueDTO, @PathVariable long id) {
		log.info("PUT Historique - " + updateHistoriqueDTO);

		Historique historique = historiqueRepository.getOne(id);
		// Mise à jour de l'historique
		historique.setDateModif(updateHistoriqueDTO.getDateModif());
		return historiqueConverter.convertToDto(historiqueRepository.save(historique));
	}

	/**
	 * Méthode DELETE de l'api RESTFul permettant de supprimer une historique
	 * 
	 * @param long id - ID de l'historique à supprimer
	 * @return ResponseEntity<HttpStatus>
	 */
	@DeleteMapping(path = "/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable long id) {
		log.info("DELETE Historique - ");
		historiqueRepository.delete(id);
	}

}

package org.polymont.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfilDTO {

	private long id;
	private String libelle;
	private String utilisateurs;
	private String privileges;

	/**
	 * @param libelle
	 */
	public ProfilDTO(String libelle, String utilisateurs, String privileges) {
		super();
		this.libelle = libelle;
		this.utilisateurs = utilisateurs;
		this.privileges = privileges;
	}

	/**
	 * @param libelle
	 */
	public ProfilDTO(String libelle) {
		super();
		this.libelle = libelle;
	}

}

package org.polymont.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupeDTO {

	private long id;
	private String libelle;
	private String utilisateurs;
	private String fichiers;
	private boolean actif;

	public GroupeDTO(String libelle, String utilisateurs, String fichiers, boolean actif) {
		super();
		this.libelle = libelle;
		this.utilisateurs = utilisateurs;
		this.fichiers = fichiers;
		this.actif = actif;
	}

}

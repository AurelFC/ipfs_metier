package org.polymont.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PrivilegeDTO {

	private Long id;
	private String libelle;

	/**
	 * @param libelle
	 * @param profils
	 */
	public PrivilegeDTO(String libelle) {
		super();
		this.libelle = libelle;
	}

}

package org.polymont.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UtilisateurDTO {

	private long id;
	private String nom;
	private String prenom;
	private String emailPro;
	private String telPro;
	private String profil;
	private boolean actif;
	private String fichiers;
	private String agences;
	private String groupes;

	public UtilisateurDTO(String nom, String prenom, String emailPro, String telPro, String profil, boolean actif,
			String fichiers, String agences, String groupes) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.emailPro = emailPro;
		this.telPro = telPro;
		this.profil = profil;
		this.actif = actif;
		this.fichiers = fichiers;
		this.agences = agences;
		this.groupes = groupes;
	}

}
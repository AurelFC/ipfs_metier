package org.polymont.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeNotificationDTO {

	private Long id;
	private String libelle;

	/**
	 * @param libelle
	 * @param notifications
	 */
	public TypeNotificationDTO(String libelle) {
		super();
		this.libelle = libelle;
	}

}

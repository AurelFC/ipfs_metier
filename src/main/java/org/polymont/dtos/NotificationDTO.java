package org.polymont.dtos;

import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationDTO {

	private Long id;
	private String contenu;
	private Calendar dateNotif;
	private boolean lue;
	private String utilisateur;
	private String typeNotification;

	/**
	 * @param contenu
	 * @param dateModif
	 * @param typeNotification
	 * @param lue
	 * @param historique
	 * @param utilisateur
	 */
	public NotificationDTO(String contenu, Calendar dateNotif, boolean lue, String utilisateur,
			String typeNotification) {
		super();
		this.contenu = contenu;
		this.dateNotif = dateNotif;
		this.lue = lue;
		this.utilisateur = utilisateur;
		this.typeNotification = typeNotification;
	}

}

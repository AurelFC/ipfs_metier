package org.polymont.dtos;

import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FichierDTO {

	private long id;
	private String nom;
	private String hash;
	private String version;
	private Calendar dateAjout;
	private String extension;
	private String owner;
	private String historique;

	/**
	 * @param nom
	 * @param hash
	 * @param version
	 * @param dateAjout
	 * @param extension
	 * @param utilisateur
	 * @param historique
	 */
	public FichierDTO(String nom, String hash, String version, Calendar dateAjout, String extension, String owner,
			String historique) {
		super();
		this.nom = nom;
		this.hash = hash;
		this.version = version;
		this.dateAjout = dateAjout;
		this.extension = extension;
		this.owner = owner;
		this.historique = historique;
	}

}
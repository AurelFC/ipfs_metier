package org.polymont.repositories;

public interface IWSReadRepository<T> {

	public T getOne(long id);

	public Iterable<T> findAll();
}

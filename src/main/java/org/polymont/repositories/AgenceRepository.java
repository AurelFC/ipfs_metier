package org.polymont.repositories;

import java.net.ConnectException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.AgenceConverter;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.AgenceDTO;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Agence;
import org.polymont.entities.Fichier;
import org.polymont.entities.Utilisateur;
import org.polymont.exceptions.GenericEntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class AgenceRepository implements IWSRepository<Agence> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	AgenceConverter agenceConverter;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	FichierConverter fichierConverter;

	public AgenceRepository() {
		super();
	}

	@Override
	public List<Agence> findAll() {
		List<Agence> listeAgences = new ArrayList<>();
		try {
			template.exchange((urlBack + Config.URI_AGENCES), HttpMethod.GET, null,
					new ParameterizedTypeReference<List<AgenceDTO>>() {
					}).getBody().forEach(agence -> {
						Agence age = agenceConverter.convertToEntity(agence);
						listeAgences.add(age);

					});
		} catch (RestClientException e) {
			if (e.getCause() instanceof ConnectException) {
				// handle connect exception
				throw new GenericEntityNotFoundException();
			}
			throw e; // rethrow if not a ConnectException
		}

		return listeAgences;
	}

	@Override
	public Agence getOne(long id) {
		AgenceDTO agenceDto = template.getForObject((urlBack + Config.URI_AGENCES + id), AgenceDTO.class);

		return agenceConverter.convertToEntity(agenceDto);
	}

	@Override
	public void delete(long id) {
		AgenceDTO agence = template.getForObject(urlBack + Config.URI_AGENCES + id, AgenceDTO.class);
		if (agence != null) {

			template.delete(urlBack + Config.URI_AGENCES + id);
		}
	}

	@Override
	public Agence save(Agence obj) {
		Agence agence;
		log.info("AgenceRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			agence = add(obj);
		} else {
			agence = update(obj);
		}
		return agence;
	}

	@Override
	public Agence add(Agence obj) {
		log.info("ADD agence -----");
		AgenceDTO agenceDto = agenceConverter.convertToDto(obj);
		agenceDto = template.postForObject(urlBack + Config.URI_AGENCES, agenceDto, AgenceDTO.class);
		return agenceConverter.convertToEntity(agenceDto);
	}

	@Override
	public Agence update(Agence obj) {
		AgenceDTO agenceDto = agenceConverter.convertToDto(obj);
		HttpEntity<AgenceDTO> requestUpdate = new HttpEntity<>(agenceDto);
		AgenceDTO agenceDtoReturn = template.exchange(urlBack + Config.URI_AGENCES + agenceDto.getId(), HttpMethod.PUT,
				requestUpdate, AgenceDTO.class).getBody();
		return agenceConverter.convertToEntity(agenceDtoReturn);
	}

	public List<Utilisateur> getUtilisateurs(long id) {
		List<Utilisateur> listeUtilisateurs = new ArrayList<>();
		template.exchange((urlBack + Config.URI_AGENCES + id + Config.URI_UTILISATEURS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UtilisateurDTO>>() {
				}).getBody().forEach(uti -> {
					Utilisateur utilisateur = utilisateurConverter.convertToEntity(uti);
					listeUtilisateurs.add(utilisateur);

				});

		return listeUtilisateurs;
	}

	public List<Fichier> getFichiers(long id) {
		List<Fichier> listeFichiers = new ArrayList<>();
		template.exchange((urlBack + Config.URI_AGENCES + id + Config.URI_FICHIERS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FichierDTO>>() {
				}).getBody().forEach(fic -> {
					Fichier fichier = fichierConverter.convertToEntity(fic);
					listeFichiers.add(fichier);

				});

		return listeFichiers;
	}
}

package org.polymont.repositories;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.NotificationConverter;
import org.polymont.dtos.NotificationDTO;
import org.polymont.entities.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class NotificationRepository implements IWSRepository<Notification> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	NotificationConverter notificationConverter;

	public NotificationRepository() {
		super();
	}

	@Override
	public List<Notification> findAll() {
		List<Notification> listeNotifications = new ArrayList<>();

		template.exchange((urlBack + Config.URI_NOTIFICATIONS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<NotificationDTO>>() {
				}).getBody().forEach(notification -> {
					Notification notif = notificationConverter.convertToEntity(notification);
					listeNotifications.add(notif);

				});
		return listeNotifications;
	}

	@Override
	public Notification getOne(long id) {
		NotificationDTO notificationDto = template.getForObject((urlBack + Config.URI_NOTIFICATIONS + id),
				NotificationDTO.class);

		return notificationConverter.convertToEntity(notificationDto);
	}

	@Override
	public void delete(long id) {
		NotificationDTO notification = template.getForObject(urlBack + Config.URI_NOTIFICATIONS + id,
				NotificationDTO.class);
		if (notification != null) {

			template.delete(urlBack + Config.URI_NOTIFICATIONS + id);
		}

	}

	@Override
	public Notification save(Notification obj) {
		Notification notification;
		log.info("NotificationRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			notification = add(obj);
		} else {
			notification = update(obj);
		}
		return notification;
	}

	@Override
	public Notification add(Notification obj) {
		log.info("ADD notification -----");
		NotificationDTO notificationDto = notificationConverter.convertToDto(obj);
		notificationDto = template.postForObject(urlBack + Config.URI_NOTIFICATIONS, notificationDto,
				NotificationDTO.class);
		return notificationConverter.convertToEntity(notificationDto);
	}

	@Override
	public Notification update(Notification obj) {
		NotificationDTO notificationDto = notificationConverter.convertToDto(obj);
		HttpEntity<NotificationDTO> requestUpdate = new HttpEntity<>(notificationDto);
		NotificationDTO notificationDtoReturn = template
				.exchange(urlBack + Config.URI_NOTIFICATIONS + notificationDto.getId(), HttpMethod.PUT, requestUpdate,
						NotificationDTO.class)
				.getBody();
		return notificationConverter.convertToEntity(notificationDtoReturn);
	}
}

package org.polymont.repositories;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.HistoriqueConverter;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.HistoriqueDTO;
import org.polymont.entities.Fichier;
import org.polymont.entities.Historique;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class FichierRepository implements IWSRepository<Fichier> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	FichierConverter fichierConverter;

	@Autowired
	HistoriqueConverter historiqueConverter;

	public FichierRepository() {
		super();
	}

	@Override
	public List<Fichier> findAll() {
		List<Fichier> listeFichiers = new ArrayList<>();

		template.exchange((urlBack + Config.URI_FICHIERS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FichierDTO>>() {
				}).getBody().forEach(fichier -> {
					Fichier fic = fichierConverter.convertToEntity(fichier);
					listeFichiers.add(fic);

				});
		return listeFichiers;
	}

	@Override
	public Fichier getOne(long id) {
		FichierDTO fichierDto = template.getForObject((urlBack + Config.URI_FICHIERS + id), FichierDTO.class);

		return fichierConverter.convertToEntity(fichierDto);
	}

	@Override
	public void delete(long id) {
		FichierDTO fichier = template.getForObject(urlBack + Config.URI_FICHIERS + id, FichierDTO.class);
		if (fichier != null) {

			template.delete(urlBack + Config.URI_FICHIERS + id);
		}

	}

	@Override
	public Fichier add(Fichier obj) {
		log.info("ADD fichier -----");
		FichierDTO fichierDto = fichierConverter.convertToDto(obj);
		fichierDto = template.postForObject(urlBack + Config.URI_FICHIERS, fichierDto, FichierDTO.class);
		return fichierConverter.convertToEntity(fichierDto);
	}

	@Override
	public Fichier update(Fichier obj) {
		FichierDTO fichierDto = fichierConverter.convertToDto(obj);
		HttpEntity<FichierDTO> requestUpdate = new HttpEntity<>(fichierDto);
		FichierDTO fichierDtoReturn = template.exchange(urlBack + Config.URI_FICHIERS + fichierDto.getId(),
				HttpMethod.PUT, requestUpdate, FichierDTO.class).getBody();
		return fichierConverter.convertToEntity(fichierDtoReturn);
	}

	@Override
	public Fichier save(Fichier obj) {
		Fichier fichier;
		log.info("FichierRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			fichier = add(obj);
		} else {
			fichier = update(obj);
		}
		return fichier;
	}

	public List<Historique> getHistoriques(long id) {
		List<Historique> listeHistoriques = new ArrayList<>();
		template.exchange((urlBack + Config.URI_FICHIERS + id + Config.URI_HISTORIQUES), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<HistoriqueDTO>>() {
				}).getBody().forEach(histo -> {
					Historique historique = historiqueConverter.convertToEntity(histo);
					listeHistoriques.add(historique);

				});

		return listeHistoriques;
	}
}

package org.polymont.repositories;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.TypeNotificationConverter;
import org.polymont.dtos.TypeNotificationDTO;
import org.polymont.entities.TypeNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class TypeNotificationRepository implements IWSRepository<TypeNotification> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	TypeNotificationConverter typeNotificationConverter;

	public TypeNotificationRepository() {
		super();
	}

	@Override
	public List<TypeNotification> findAll() {
		List<TypeNotification> listetypeNotifications = new ArrayList<>();

		template.exchange((urlBack + Config.URI_TYPENOTIFICATION), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TypeNotificationDTO>>() {
				}).getBody().forEach(typeNotification -> {
					TypeNotification typeNo = typeNotificationConverter.convertToEntity(typeNotification);
					listetypeNotifications.add(typeNo);

				});
		return listetypeNotifications;
	}

	@Override
	public TypeNotification getOne(long id) {
		TypeNotificationDTO typeNotificationDto = template.getForObject((urlBack + Config.URI_TYPENOTIFICATION + id),
				TypeNotificationDTO.class);

		return typeNotificationConverter.convertToEntity(typeNotificationDto);
	}

	@Override
	public void delete(long id) {
		TypeNotificationDTO typeNotification = template.getForObject(urlBack + Config.URI_TYPENOTIFICATION + id,
				TypeNotificationDTO.class);
		if (typeNotification != null) {

			template.delete(urlBack + Config.URI_TYPENOTIFICATION + id);
		}
	}

	@Override
	public TypeNotification add(TypeNotification obj) {
		log.info("ADD typeNotification -----");
		TypeNotificationDTO typeNotificationDto = typeNotificationConverter.convertToDto(obj);
		typeNotificationDto = template.postForObject(urlBack + Config.URI_TYPENOTIFICATION, typeNotificationDto,
				TypeNotificationDTO.class);
		return typeNotificationConverter.convertToEntity(typeNotificationDto);
	}

	@Override
	public TypeNotification update(TypeNotification obj) {
		TypeNotificationDTO typeNotificationDto = typeNotificationConverter.convertToDto(obj);
		HttpEntity<TypeNotificationDTO> requestUpdate = new HttpEntity<>(typeNotificationDto);
		TypeNotificationDTO typeNotificationDtoReturn = template
				.exchange(urlBack + Config.URI_TYPENOTIFICATION + typeNotificationDto.getId(), HttpMethod.PUT,
						requestUpdate, TypeNotificationDTO.class)
				.getBody();
		return typeNotificationConverter.convertToEntity(typeNotificationDtoReturn);
	}

	@Override
	public TypeNotification save(TypeNotification obj) {
		TypeNotification typeNotification;
		log.info("TypeNotificationRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			typeNotification = add(obj);
		} else {
			typeNotification = update(obj);
		}
		return typeNotification;
	}

}

package org.polymont.repositories;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.PrivilegeConverter;
import org.polymont.dtos.PrivilegeDTO;
import org.polymont.entities.Privilege;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class PrivilegeRepository implements IWSRepository<Privilege> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	PrivilegeConverter privilegeConverter;

	public PrivilegeRepository() {
		super();
	}

	@Override
	public List<Privilege> findAll() {
		List<Privilege> listePrivileges = new ArrayList<>();

		template.exchange((urlBack + Config.URI_PRIVILEGES), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PrivilegeDTO>>() {
				}).getBody().forEach(privilege -> {
					Privilege priv = privilegeConverter.convertToEntity(privilege);
					listePrivileges.add(priv);

				});
		return listePrivileges;
	}

	@Override
	public Privilege getOne(long id) {
		PrivilegeDTO privilegeDto = template.getForObject((urlBack + Config.URI_PRIVILEGES + id), PrivilegeDTO.class);

		return privilegeConverter.convertToEntity(privilegeDto);
	}

	@Override
	public Privilege add(Privilege obj) {
		log.info("ADD privilege -----");
		PrivilegeDTO privilegeDto = privilegeConverter.convertToDto(obj);
		privilegeDto = template.postForObject(urlBack + Config.URI_PRIVILEGES, privilegeDto, PrivilegeDTO.class);
		return privilegeConverter.convertToEntity(privilegeDto);
	}

	@Override
	public Privilege update(Privilege obj) {
		PrivilegeDTO privilegeDto = privilegeConverter.convertToDto(obj);
		HttpEntity<PrivilegeDTO> requestUpdate = new HttpEntity<>(privilegeDto);
		PrivilegeDTO privilegeDtoReturn = template.exchange(urlBack + Config.URI_PRIVILEGES + privilegeDto.getId(),
				HttpMethod.PUT, requestUpdate, PrivilegeDTO.class).getBody();
		return privilegeConverter.convertToEntity(privilegeDtoReturn);
	}

	@Override
	public void delete(long id) {
		PrivilegeDTO privilege = template.getForObject(urlBack + Config.URI_PRIVILEGES + id, PrivilegeDTO.class);
		if (privilege != null) {

			template.delete(urlBack + Config.URI_PRIVILEGES + id);
		}
	}

	@Override
	public Privilege save(Privilege obj) {
		Privilege privilege;
		log.info("PrivilegeRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			privilege = add(obj);
		} else {
			privilege = update(obj);
		}
		return privilege;
	}

}

package org.polymont.repositories;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.HistoriqueConverter;
import org.polymont.dtos.HistoriqueDTO;
import org.polymont.entities.Historique;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class HistoriqueRepository implements IWSRepository<Historique> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	HistoriqueConverter historiqueConverter;

	public HistoriqueRepository() {
		super();
	}

	@Override
	public List<Historique> findAll() {
		List<Historique> listeHistoriques = new ArrayList<>();

		template.exchange((urlBack + Config.URI_HISTORIQUES), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<HistoriqueDTO>>() {
				}).getBody().forEach(historique -> {
					Historique histo = historiqueConverter.convertToEntity(historique);
					listeHistoriques.add(histo);

				});
		return listeHistoriques;
	}

	@Override
	public Historique getOne(long id) {
		HistoriqueDTO historiqueDto = template.getForObject((urlBack + Config.URI_HISTORIQUES + id),
				HistoriqueDTO.class);

		return historiqueConverter.convertToEntity(historiqueDto);
	}

	@Override
	public void delete(long id) {
		HistoriqueDTO historique = template.getForObject(urlBack + Config.URI_HISTORIQUES + id, HistoriqueDTO.class);
		if (historique != null) {

			template.delete(urlBack + Config.URI_HISTORIQUES + id);
		}

	}

	@Override
	public Historique add(Historique obj) {
		log.info("ADD historique -----");
		HistoriqueDTO historiqueDto = historiqueConverter.convertToDto(obj);
		historiqueDto = template.postForObject(urlBack + Config.URI_UTILISATEURS, historiqueDto, HistoriqueDTO.class);
		return historiqueConverter.convertToEntity(historiqueDto);
	}

	@Override
	public Historique update(Historique obj) {
		HistoriqueDTO historiqueDto = historiqueConverter.convertToDto(obj);
		HttpEntity<HistoriqueDTO> requestUpdate = new HttpEntity<>(historiqueDto);
		HistoriqueDTO historiqueDtoReturn = template.exchange(urlBack + Config.URI_HISTORIQUES + historiqueDto.getId(),
				HttpMethod.PUT, requestUpdate, HistoriqueDTO.class).getBody();
		return historiqueConverter.convertToEntity(historiqueDtoReturn);
	}

	@Override
	public Historique save(Historique obj) {
		Historique historique;
		log.info("HistoriqueRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			historique = add(obj);
		} else {
			historique = update(obj);
		}
		return historique;
	}
}

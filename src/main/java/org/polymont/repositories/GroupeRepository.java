package org.polymont.repositories;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.GroupeConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.GroupeDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Fichier;
import org.polymont.entities.Groupe;
import org.polymont.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class GroupeRepository implements IWSRepository<Groupe> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	GroupeConverter groupeConverter;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	FichierConverter fichierConverter;

	public GroupeRepository() {
		super();
	}

	@Override
	public List<Groupe> findAll() {
		List<Groupe> listegroupes = new ArrayList<>();

		template.exchange((urlBack + Config.URI_GROUPES), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GroupeDTO>>() {
				}).getBody().forEach(groupe -> {
					Groupe gro = groupeConverter.convertToEntity(groupe);
					listegroupes.add(gro);

				});
		return listegroupes;
	}

	@Override
	public Groupe getOne(long id) {
		GroupeDTO groupeDto = template.getForObject((urlBack + Config.URI_GROUPES + id), GroupeDTO.class);

		return groupeConverter.convertToEntity(groupeDto);
	}

	@Override
	public void delete(long id) {
		GroupeDTO groupe = template.getForObject(urlBack + Config.URI_GROUPES + id, GroupeDTO.class);
		if (groupe != null) {

			template.delete(urlBack + Config.URI_GROUPES + id);
		}
	}

	@Override
	public Groupe save(Groupe obj) {
		Groupe groupe;
		log.info("GroupeRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			groupe = add(obj);
		} else {
			groupe = update(obj);
		}
		return groupe;
	}

	@Override
	public Groupe add(Groupe obj) {
		log.info("ADD groupe -----");
		GroupeDTO groupeDto = groupeConverter.convertToDto(obj);
		groupeDto = template.postForObject(urlBack + Config.URI_GROUPES, groupeDto, GroupeDTO.class);
		return groupeConverter.convertToEntity(groupeDto);
	}

	@Override
	public Groupe update(Groupe obj) {
		GroupeDTO groupeDto = groupeConverter.convertToDto(obj);
		HttpEntity<GroupeDTO> requestUpdate = new HttpEntity<>(groupeDto);
		GroupeDTO groupeDtoReturn = template.exchange(urlBack + Config.URI_GROUPES + groupeDto.getId(), HttpMethod.PUT,
				requestUpdate, GroupeDTO.class).getBody();
		return groupeConverter.convertToEntity(groupeDtoReturn);
	}

	public List<Utilisateur> getUtilisateurs(long id) {
		List<Utilisateur> listeUtilisateurs = new ArrayList<>();
		template.exchange((urlBack + Config.URI_GROUPES + id + Config.URI_UTILISATEURS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UtilisateurDTO>>() {
				}).getBody().forEach(uti -> {
					Utilisateur utilisateur = utilisateurConverter.convertToEntity(uti);
					listeUtilisateurs.add(utilisateur);

				});

		return listeUtilisateurs;
	}

	public List<Fichier> getFichiers(long id) {
		List<Fichier> listeFichiers = new ArrayList<>();
		template.exchange((urlBack + Config.URI_GROUPES + id + Config.URI_FICHIERS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FichierDTO>>() {
				}).getBody().forEach(fic -> {
					Fichier fichier = fichierConverter.convertToEntity(fic);
					listeFichiers.add(fichier);

				});

		return listeFichiers;
	}
}

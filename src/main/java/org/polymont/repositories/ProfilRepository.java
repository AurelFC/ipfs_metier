package org.polymont.repositories;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.PrivilegeConverter;
import org.polymont.converter.ProfilConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.PrivilegeDTO;
import org.polymont.dtos.ProfilDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Privilege;
import org.polymont.entities.Profil;
import org.polymont.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class ProfilRepository implements IWSRepository<Profil> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	ProfilConverter profilConverter;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	PrivilegeConverter privilegeConverter;

	public ProfilRepository() {
		super();
	}

	@Override
	public List<Profil> findAll() {
		List<Profil> listeProfils = new ArrayList<>();

		template.exchange((urlBack + Config.URI_PROFILS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProfilDTO>>() {
				}).getBody().forEach(profil -> {
					Profil prof = profilConverter.convertToEntity(profil);
					listeProfils.add(prof);

				});
		return listeProfils;
	}

	@Override
	public Profil getOne(long id) {
		ProfilDTO profilDto = template.getForObject((urlBack + Config.URI_PROFILS + id), ProfilDTO.class);

		return profilConverter.convertToEntity(profilDto);
	}

	@Override
	public Profil add(Profil obj) {
		log.info("ADD profil -----");
		ProfilDTO profilDto = profilConverter.convertToDto(obj);
		profilDto = template.postForObject(urlBack + Config.URI_PROFILS, profilDto, ProfilDTO.class);
		return profilConverter.convertToEntity(profilDto);
	}

	@Override
	public Profil update(Profil obj) {
		ProfilDTO profilDto = profilConverter.convertToDto(obj);
		HttpEntity<ProfilDTO> requestUpdate = new HttpEntity<>(profilDto);
		ProfilDTO profilDtoReturn = template.exchange(urlBack + Config.URI_PROFILS + profilDto.getId(),
				HttpMethod.PUT, requestUpdate, ProfilDTO.class).getBody();
		return profilConverter.convertToEntity(profilDtoReturn);
	}

	@Override
	public void delete(long id) {
		ProfilDTO profil = template.getForObject(urlBack + Config.URI_PROFILS + id, ProfilDTO.class);
		if (profil != null) {

			template.delete(urlBack + Config.URI_PROFILS + id);
		}
	}

	@Override
	public Profil save(Profil obj) {
		Profil profil;
		log.info("ProfilRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			profil = add(obj);
		} else {
			profil = update(obj);
		}
		return profil;
	}

	public List<Utilisateur> getUtilisateurs(long id) {
		List<Utilisateur> listeUtilisateurs = new ArrayList<>();
		template.exchange((urlBack + Config.URI_PROFILS + id + Config.URI_UTILISATEURS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UtilisateurDTO>>() {
				}).getBody().forEach(uti -> {
					Utilisateur utilisateur = utilisateurConverter.convertToEntity(uti);
					listeUtilisateurs.add(utilisateur);

				});

		return listeUtilisateurs;
	}

	public List<Privilege> getPrivileges(long id) {
		List<Privilege> listePrivileges = new ArrayList<>();
		template.exchange((urlBack + Config.URI_PROFILS + id + Config.URI_PRIVILEGES), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PrivilegeDTO>>() {
				}).getBody().forEach(priv -> {
					Privilege privilege = privilegeConverter.convertToEntity(priv);
					listePrivileges.add(privilege);

				});

		return listePrivileges;
	}

}

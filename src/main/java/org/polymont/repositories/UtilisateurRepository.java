package org.polymont.repositories;

import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;
import org.polymont.converter.AgenceConverter;
import org.polymont.converter.FichierConverter;
import org.polymont.converter.GroupeConverter;
import org.polymont.converter.NotificationConverter;
import org.polymont.converter.ProfilConverter;
import org.polymont.converter.UtilisateurConverter;
import org.polymont.dtos.AgenceDTO;
import org.polymont.dtos.FichierDTO;
import org.polymont.dtos.GroupeDTO;
import org.polymont.dtos.NotificationDTO;
import org.polymont.dtos.UtilisateurDTO;
import org.polymont.entities.Agence;
import org.polymont.entities.Fichier;
import org.polymont.entities.Groupe;
import org.polymont.entities.Notification;
import org.polymont.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Repository
public class UtilisateurRepository implements IWSRepository<Utilisateur> {

	@Autowired
	private RestTemplate template;

	@Value("${url.controller}")
	private String urlBack;

	@Autowired
	UtilisateurConverter utilisateurConverter;

	@Autowired
	ProfilConverter profilConverter;

	@Autowired
	AgenceConverter agenceConverter;

	@Autowired
	GroupeConverter groupeConverter;

	@Autowired
	NotificationConverter notificationConverter;

	@Autowired
	FichierConverter fichierConverter;

	public UtilisateurRepository() {
		super();
	}

	@Override
	public List<Utilisateur> findAll() {

		List<Utilisateur> listeUtilisateurs = new ArrayList<>();

		template.exchange((urlBack + Config.URI_UTILISATEURS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UtilisateurDTO>>() {
				}).getBody().forEach(utilisateur -> {
					Utilisateur util = utilisateurConverter.convertToEntity(utilisateur);
					listeUtilisateurs.add(util);

				});
		return listeUtilisateurs;
	}

	public Utilisateur findByEmailPro(String email) {
		List<Utilisateur> listeUtilisateurs = new ArrayList<>();

		template.exchange((urlBack + Config.URI_UTILISATEURS + "?email=" + email + "@polymont.fr"), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UtilisateurDTO>>() {

				}).getBody().forEach(utilisateur -> {
					Utilisateur util = utilisateurConverter.convertToEntity(utilisateur);
					listeUtilisateurs.add(util);

				});

		return listeUtilisateurs.get(0);
	}

	@Override
	public Utilisateur getOne(long id) {

		UtilisateurDTO utilisateurDto = template.getForObject((urlBack + Config.URI_UTILISATEURS + id),
				UtilisateurDTO.class);

		return utilisateurConverter.convertToEntity(utilisateurDto);
	}

	@Override
	public Utilisateur add(Utilisateur obj) {
		log.info("ADD utilisateur METIER -----");
		UtilisateurDTO utilisateurDto = utilisateurConverter.convertToDto(obj);
		utilisateurDto = template.postForObject(urlBack + Config.URI_UTILISATEURS, utilisateurDto,
				UtilisateurDTO.class);
		return utilisateurConverter.convertToEntity(utilisateurDto);

	}

	@Override
	public Utilisateur update(Utilisateur obj) {
		UtilisateurDTO utilisateurDto = utilisateurConverter.convertToDto(obj);
		HttpEntity<UtilisateurDTO> requestUpdate = new HttpEntity<>(utilisateurDto);
		UtilisateurDTO utilisateurDtoReturn = template
				.exchange(urlBack + Config.URI_UTILISATEURS + utilisateurDto.getId(), HttpMethod.PUT, requestUpdate,
						UtilisateurDTO.class)
				.getBody();
		return utilisateurConverter.convertToEntity(utilisateurDtoReturn);
	}

	@Override
	public void delete(long id) {

		UtilisateurDTO utilisateur = template.getForObject(urlBack + Config.URI_UTILISATEURS + id,
				UtilisateurDTO.class);
		if (utilisateur != null) {

			template.delete(urlBack + Config.URI_UTILISATEURS + id);
		}
	}

	@Override
	public Utilisateur save(Utilisateur obj) {
		Utilisateur util;
		log.info("UtilisateurRepository---------------------------");
		log.info(obj);
		if (obj.getId() == 0) {
			util = add(obj);
		} else {
			util = update(obj);
		}
		return util;
	}

	/**
	 * GET : Retourne les agences de l'utilisateur
	 * 
	 * @Route /utilisateurs/{id}/agences
	 * @param id
	 * @return List<Agence>
	 */
	public List<Agence> getAgences(long id) {
		List<Agence> listeAgences = new ArrayList<>();
		template.exchange((urlBack + Config.URI_UTILISATEURS + id + Config.URI_AGENCES), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AgenceDTO>>() {
				}).getBody().forEach(age -> {
					Agence agence = agenceConverter.convertToEntity(age);
					listeAgences.add(agence);

				});

		return listeAgences;
	}

	/**
	 * GET : Retourne les groupes de l'utilisateur
	 * 
	 * @Route /utilisateurs/{id}/groupes
	 * @param id
	 * @return List<Groupe>
	 */
	public List<Groupe> getGroupes(long id) {
		List<Groupe> listeGroupes = new ArrayList<>();
		template.exchange((urlBack + Config.URI_UTILISATEURS + id + Config.URI_GROUPES), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GroupeDTO>>() {
				}).getBody().forEach(gro -> {
					Groupe groupe = groupeConverter.convertToEntity(gro);
					listeGroupes.add(groupe);

				});

		return listeGroupes;
	}

	/**
	 * GET : Retourne les notifications de l'utilisateur
	 * 
	 * @Route /utilisateurs/{id}/notifications
	 * @param id
	 * @return List<Notification>
	 */
	public List<Notification> getNotifications(long id) {
		List<Notification> listeNotifications = new ArrayList<>();
		template.exchange((urlBack + Config.URI_UTILISATEURS + id + Config.URI_NOTIFICATIONS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<NotificationDTO>>() {
				}).getBody().forEach(notif -> {
					Notification notification = notificationConverter.convertToEntity(notif);
					listeNotifications.add(notification);

				});

		return listeNotifications;
	}

	/**
	 * GET : Retourne les fichiers de l'utilisateur
	 * 
	 * @Route /utilisateurs/{id}/fichiers
	 * @param id
	 * @return List<Fichier>
	 */
	public List<Fichier> getFichiers(long id) {
		List<Fichier> listeFichiers = new ArrayList<>();
		template.exchange((urlBack + Config.URI_UTILISATEURS + id + Config.URI_FICHIERS), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FichierDTO>>() {
				}).getBody().forEach(fic -> {
					Fichier fichier = fichierConverter.convertToEntity(fic);
					listeFichiers.add(fichier);

				});

		return listeFichiers;
	}

	/**
	 * PUT : Permet d'ajouter une agence à l'utilisateur
	 * 
	 * @Route /utilisateurs/{id}/agences
	 * @param id
	 * @param agence
	 * @return UtilisateurDTO
	 */
	public Utilisateur addAgence(long id, Agence agence) {
		AgenceDTO agenceDto = agenceConverter.convertToDto(agence);
		log.info("agencedto " + agenceDto);
		HttpEntity<AgenceDTO> requestUpdate = new HttpEntity<>(agenceDto);
		UtilisateurDTO utilisateurDto = template.exchange(urlBack + Config.URI_UTILISATEURS + id + Config.URI_AGENCES,
				HttpMethod.PUT, requestUpdate, UtilisateurDTO.class).getBody();
		return utilisateurConverter.convertToEntity(utilisateurDto);
	}

	/**
	 * PUT : Permet d'ajouter un groupe à l'utilisateur
	 * 
	 * @Route /utilisateurs/{id}/groupes
	 * @param id
	 * @param groupe
	 * @return UtilisateurDTO
	 */
	public Utilisateur addGroupe(long id, Groupe groupe) {
		GroupeDTO groupeDto = groupeConverter.convertToDto(groupe);
		log.info("groupeDto " + groupeDto);
		HttpEntity<GroupeDTO> requestUpdate = new HttpEntity<>(groupeDto);
		UtilisateurDTO utilisateurDto = template.exchange(urlBack + Config.URI_UTILISATEURS + id + Config.URI_GROUPES,
				HttpMethod.PUT, requestUpdate, UtilisateurDTO.class).getBody();
		return utilisateurConverter.convertToEntity(utilisateurDto);
	}

	public Groupe deleteGroupe(long id, long id2) {

		GroupeDTO groupeDto = template.exchange((urlBack + Config.URI_UTILISATEURS + id + Config.URI_GROUPES + id2),
				HttpMethod.PUT, null, GroupeDTO.class).getBody();

		return groupeConverter.convertToEntity(groupeDto);
	}

	public Agence deleteAgence(long id, long id2) {

		AgenceDTO agenceDto = template.exchange((urlBack + Config.URI_UTILISATEURS + id + Config.URI_AGENCES + id2),
				HttpMethod.PUT, null, AgenceDTO.class).getBody();

		return agenceConverter.convertToEntity(agenceDto);
	}

}

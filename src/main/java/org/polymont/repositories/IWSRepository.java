package org.polymont.repositories;

/**
 * Interface de connexion nécessaire pour hibernate
 * date dernière modif : 02-05-2018
 * 
 */

public interface IWSRepository<T> extends IWSReadRepository<T>{
	
	public void delete(long id);
	
	public T save(T obj);
	
	public T add(T obj);
	
	public T update(T obj);
	
}

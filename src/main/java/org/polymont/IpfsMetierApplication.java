package org.polymont;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
public class IpfsMetierApplication {
	public static void main(String[] args) {
		SpringApplication.run(IpfsMetierApplication.class, args);
	}
}
package org.polymont.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Profil implements Serializable {

	private static final long serialVersionUID = -1491932107959856393L;

	private long id;

	private String libelle;

	private List<Utilisateur> utilisateurs;

	private List<Privilege> privileges;

	/**
	 * @param libelle
	 * @param utilisateurs
	 * @param privileges
	 */
	public Profil(@NotBlank String libelle, List<Utilisateur> utilisateurs, List<Privilege> privileges) {
		super();
		this.libelle = libelle;
		if (utilisateurs != null) {
			this.utilisateurs = utilisateurs;
		} else {
			this.utilisateurs = new ArrayList<>();
		}
		if (privileges != null) {
			this.privileges = privileges;
		} else {
			this.privileges = new ArrayList<>();
		}
	}

	/**
	 * @param libelle
	 */
	public Profil(@NotBlank String libelle) {
		super();
		this.libelle = libelle;
	}

	public void addUtilisateurs(Utilisateur utilisateur) {
		if (this.utilisateurs == null) {
			this.utilisateurs = new ArrayList<>();
		}

		this.utilisateurs.add(utilisateur);
	}

	public void addPrivileges(Privilege privilege) {
		if (this.privileges == null) {
			this.privileges = new ArrayList<>();
		}

		this.privileges.add(privilege);
	}

	public String toString() {
		return Config.URI_PROFILS + this.id;
	}

}

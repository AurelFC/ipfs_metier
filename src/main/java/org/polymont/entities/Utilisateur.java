package org.polymont.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Utilisateur extends Owner implements Serializable {

	private static final long serialVersionUID = 1291913906682762873L;

	private String nom;

	private String prenom;

	private String emailPro;

	private String telPro;

	private Profil profil;

	private List<Groupe> groupes;

	private List<Agence> agences;

	public void addGroupes(Groupe groupe) {
		if (this.groupes == null) {
			this.groupes = new ArrayList<>();
		}

		this.groupes.add(groupe);
	}

	public void addAgences(Agence agence) {
		if (this.agences == null) {
			this.agences = new ArrayList<>();
		}

		this.agences.add(agence);
	}

	@Override
	public String toString() {
		return Config.URI_UTILISATEURS + this.getId();
	}

}

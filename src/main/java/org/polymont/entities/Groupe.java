package org.polymont.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Groupe extends Owner implements Serializable {

	private static final long serialVersionUID = -5097351887744179835L;


	private String libelle;

	private List<Utilisateur> utilisateurs;


	public void addUtilisateurs(Utilisateur utilisateur) {
		if (this.utilisateurs == null) {
			this.utilisateurs = new ArrayList<>();
		}

		this.utilisateurs.add(utilisateur);
	}

	public void deleteUtilisateurs(Utilisateur utilisateur) {
		if (this.utilisateurs != null) {
			this.utilisateurs.remove(utilisateur);
		}
	}

	@Override
	public String toString() {
		return Config.URI_GROUPES + this.getId();
	}

}

package org.polymont.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Owner implements Serializable {

	private static final long serialVersionUID = -4600850262305573228L;

	private long id;

	private boolean actif;

}

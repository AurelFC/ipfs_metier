package org.polymont.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Historique implements Serializable {

	private static final long serialVersionUID = -1593683117096260510L;

	private long id;

	private Calendar dateModif;

	private String libelle;

	Fichier fichier;

	/**
	 * @param dateModif
	 * @param libelle
	 * @param fichier
	 */
	public Historique(@NotNull Calendar dateModif, @NotBlank String libelle, Fichier fichier) {
		super();
		this.dateModif = dateModif;
		this.libelle = libelle;
		this.fichier = fichier;
	}

	@Override
	public String toString() {
		return Config.URI_HISTORIQUES + this.id;
	}

}

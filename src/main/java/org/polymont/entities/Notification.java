package org.polymont.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notification implements Serializable {

	private static final long serialVersionUID = -746599162104724455L;

	private long id;

	private String contenu;

	private Calendar dateNotif;

	private boolean lue;

	private TypeNotification typeNotification;

	private Utilisateur utilisateur;

	/**
	 * @param contenu
	 * @param dateNotif
	 * @param lue
	 * @param typeNotification
	 * @param utilisateur
	 */
	public Notification(@NotBlank String contenu, @NotNull Calendar dateNotif, @NotNull boolean lue,
			TypeNotification typeNotification, Utilisateur utilisateur) {
		super();
		this.contenu = contenu;
		this.dateNotif = dateNotif;
		this.lue = lue;
		this.typeNotification = typeNotification;
		this.utilisateur = utilisateur;
	}

	public String toString() {
		return Config.URI_NOTIFICATIONS + this.id;
	}
}

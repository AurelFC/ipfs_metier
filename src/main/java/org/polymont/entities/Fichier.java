package org.polymont.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Fichier implements Serializable {

	private static final long serialVersionUID = 228831875691089728L;

	private long id;
	private String nom;

	private String hash;

	private String version;

	private Calendar dateAjout;

	private String extension;

	private Owner owner;

	private List<Historique> historiques;

	/**
	 * @param nom
	 * @param hash
	 * @param version
	 * @param dateAjout
	 * @param extension
	 * @param utilisateur
	 * @param historiques
	 */
	public Fichier(@NotBlank String nom, @NotBlank String hash, @NotBlank String version, Calendar dateAjout,
			@NotBlank String extension, Owner owner, List<Historique> historiques) {
		super();
		this.nom = nom;
		this.hash = hash;
		this.version = version;
		this.dateAjout = dateAjout;
		this.extension = extension;
		this.owner = owner;

		if (historiques != null) {
			this.historiques = historiques;
		} else {
			this.historiques = new ArrayList<>();
		}
	}

	@Override
	public String toString() {
		return Config.URI_FICHIERS + this.id;
	}

	public void addHistorique(Historique histo) {
		if (this.historiques == null) {
			this.historiques = new ArrayList<>();
		}

		this.historiques.add(histo);
	}
}

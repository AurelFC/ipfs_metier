package org.polymont.entities;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import org.polymont.configuration.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Privilege implements Serializable {

	private static final long serialVersionUID = 4564972585748144588L;

	private long id;

	private String libelle;

	/**
	 * @param libelle
	 * @param profils
	 */
	public Privilege(@NotBlank String libelle) {
		super();
		this.libelle = libelle;
	}

	public String toString() {
		return Config.URI_PRIVILEGES + this.id;
	}
}
